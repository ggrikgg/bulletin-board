class Category < ApplicationRecord
  has_many :subcategories, class_name: 'Category', foreign_key: 'parent_id', dependent: :destroy
  belongs_to :parent_category, class_name: 'Category', optional: true
  has_many   :items

  validates :name, presence: true, length: { maximum: 40 }

  before_save :downcase_fields

  def downcase_fields
    name.downcase!
  end

  def self.test_draw
    @root_category = Category.find(1)
    dashstr = '-'
    @@counter = 0
    temp = Array.new(Category.count)
    temp[@@counter] = Array.new(3)
    temp[@@counter][0] = @root_category.id
    temp[@@counter][1] = @root_category.name
    if @root_category.subcategories.any?
      temp[@@counter][2] = true
      @@counter += 1
      draw_tree(@root_category, dashstr, temp)
    else
      temp[@@counter][2] = false
      @@counter += 1
    end
    temp
  end

  def self.draw_tree(category, dashstr, temp)
    category.subcategories.each do |child|
      temp[@@counter] = Array.new(3)
      temp[@@counter][0] = child.id
      temp[@@counter][1] = dashstr + child.name
      if child.subcategories.any?
        temp[@@counter][2] = true
        @@counter += 1
        draw_tree(child, dashstr + '-', temp)
      else
        temp[@@counter][2] = false
        @@counter += 1
      end
    end; nil
  end

  #  validates_presence_of :parent_category, :name => "Should be present"
end
