# frozen_string_literal: true

class Item < ApplicationRecord
  mount_uploaders :attachments, AttachmentsUploader

  belongs_to :user
  delegate :name, :price, :description, :avatar, :created_at, to: :user, prefix: true
  belongs_to :category

  validates :user_id, :price, presence: true
  validates :name, presence: true, length: { maximum: 40 }
  validates :description, presence: true, length: { maximum: 140 }
  validates :price, numericality: { greater_than_or_equal_to: 0 }

  def self.test_draw
    @root_category = Category.find(1)
    dashstr = '-'
    @@counter = 0
    temp = Array.new(Category.count - 1)
    @root_category.subcategories.each do |node|
      temp[@@counter] = Array.new(3)
      temp[@@counter][0] = node.id
      temp[@@counter][1] = node.name
      if node.subcategories.any?
        temp[@@counter][2] = true
        @@counter += 1
        draw_tree(node, dashstr, temp)
      else
        temp[@@counter][2] = false
        @@counter += 1
      end
    end
    temp
  end

  def self.draw_tree(category, dashstr, temp)
    category.subcategories.each do |child|
      temp[@@counter] = Array.new(3)
      temp[@@counter][0] = child.id
      temp[@@counter][1] = dashstr + child.name
      if child.subcategories.any?
        temp[@@counter][2] = true
        @@counter += 1
        draw_tree(child, dashstr + '-', temp)
      else
        temp[@@counter][2] = false
        @@counter += 1
      end
    end; nil
  end
end
