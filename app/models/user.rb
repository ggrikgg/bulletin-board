# frozen_string_literal: true

class User < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :rememberable, :trackable,
         :omniauthable, omniauth_providers: [:vkontakte]

  has_many :items, dependent: :destroy

  has_many :sent_private_messages, class_name: 'Message', foreign_key: 'sender_id'
  has_many :received_private_messages, class_name: 'Message', foreign_key: 'receiver_id'

  def self.find_for_vkontakte_oauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.name = auth.info.name
      user.remote_avatar_url = auth.info.image
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
    end
  end
end
