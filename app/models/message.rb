class Message < ActiveRecord::Base
  belongs_to :sender, class_name: 'User', optional: true
  delegate :name, :avatar, to: :sender, prefix: true

  belongs_to :receiver, class_name: 'User'
  delegate :avatar, to: :receiver, prefix: true

  validates :receiver, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP },
                    presence: true, unless: ->(message) { message.sender_id.present? }
  validates :body, presence: true, length: { maximum: 140 }
end
