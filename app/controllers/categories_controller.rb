class CategoriesController < ApplicationController
  before_action :admin_user, only: %i[create new]

  @@category_hash = nil

  def index; end

  def new
    @category = Category.new
    @categtree = Category.test_draw
  end

  def categ_index
    page = params[:path].downcase.split('/')
    category = root_category
    page.each do |x|
      unless category = category.subcategories.find_by(name: x)
        break
      end
    end

    if category.nil?
      @items = Item.order(created_at: :DESC).paginate(page: params[:page], per_page: 3)
      flash.notice = 'Неверная категория'
    else
      @items = Item.where(category_id: category.id).order(created_at: :DESC).paginate(page: params[:page], per_page: 3)
    end
  end

  def create
    @category = Category.new(category_params)
    @categtree = Category.test_draw
    respond_to do |format|
      if @category.save
        @@category_hash = tree_to_hash
        format.html { redirect_to categories_path, success: 'Категория успешно создана.' }
        format.json { render :show, status: :created, location: categories_path }
      else
        format.html { render :new, info: 'Не получилось создать категорию.' }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  helper_method :category_hash

  def category_hash
    @@category_hash ||= tree_to_hash
  end

  private

  def tree_to_hash
    hash = {}

    route = '/categories'
    if root_category.subcategories.any?
      root_category.subcategories.each do |child|
        hash.merge!(nested_hash(child, route + '/' + child.name))
      end
    end
    hash
  end

  def nested_hash(node, route)
    hash = {}
    temp_hash = {}
    if node.subcategories.any?
      node.subcategories.each do |child|
        temp_hash.merge!(nested_hash(child, route + '/' + child.name))
      end
      hash[{ node.name => route }] = temp_hash
    else
      hash[node.name] = route
    end
    hash
  end

  def category_params
    params.require(:category).permit(:name, :parent_id)
  end

  def root_category
    @root_category ||= Category.find(1)
  end
end
