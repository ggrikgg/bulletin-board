# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :admin_user, only: %i[destroy change index ban unban admin]

  def show
    @user = User.includes(:items).find(params[:id])
    if  (params[:active_page].nil? && params[:finished_page].nil?)
      @active_items = @user.items.where(sold: false).order(created_at: :DESC).paginate(page: params[:active_page], per_page: 16)
      params[:active_page] = 1 if params[:active_page].nil?
      params[:active_page] =params[:active_page].to_i + 1
      @finished_items = @user.items.where(sold: true).order(created_at: :DESC).paginate(page: params[:finished_page], per_page: 16)
      params[:finished_page] = 1 if params[:finished_page].nil?
      params[:finished_page] = params[:finished_page].to_i + 1
    else
      if (params[:active_page].nil?)
        @finished_items = @user.items.where(sold: true).order(created_at: :DESC).paginate(page: params[:finished_page], per_page: 16)
        params[:finished_page] = 1 if params[:finished_page].nil?
        params[:finished_page] = params[:finished_page].to_i + 1
      else
        @active_items = @user.items.where(sold: false).order(created_at: :DESC).paginate(page: params[:active_page], per_page: 16)
        params[:active_page] = 1 if params[:active_page].nil?
        params[:active_page] = params[:active_page].to_i + 1
      end
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def index
    @users = User.all.order(created_at: :DESC).paginate(page: params[:page], per_page: 10)
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'Пользователь удалён'
    redirect_to users_url
  end

  #   def update
  #     @user = User.find(params[:id])
  #     respond_to do |format|
  #       if @user.update(user_params)
  #         format.html { redirect_to @user, notice: 'Объявление успешно изменено.' }
  #         format.json { render :show, status: :ok, location: @user }
  #       else
  #         format.html { redirect_to @user, notice: 'Объявление не было успешно изменено.' }
  #         format.json { render json: @user.errors, status: :unprocessable_entity }
  #       end
  #     end
  #   end

  def ban
    @user = User.find(params[:id])
    @user.update(ban: true)
    redirect_to @user, success: 'Пользователь был забанен.'
  end

  def unban
    @user = User.find(params[:id])
    @user.update(ban: false)
    redirect_to @user, success: 'Пользователь был разбанен.'
  end

  def admin
    @user = User.find(params[:id])
    @user.update(admin: true)
    redirect_to @user, success: 'Пользователь стал администратором.'
  end

  def change
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, info: 'Изменения сохранены.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { redirect_to @user, danger: 'Не удалось сохранить изменения.' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_params
    params.permit(:ban, :admin)
  end
end
