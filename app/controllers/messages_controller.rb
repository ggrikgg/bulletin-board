# frozen_string_literal: true

class MessagesController < ApplicationController
  def index
    @inmessages = current_user.received_private_messages.includes(:sender).order(created_at: :DESC).paginate(page: params[:page], per_page: 5)
  end

  def index_outbox
    @outmessages = current_user.sent_private_messages.includes(:receiver, :sender).order(created_at: :DESC).paginate(page: params[:page], per_page: 5)
  end

  # GET /items/1
  # GET /items/1.json
  def show; end

  # GET /items/new
  def new
    receiver = User.where(id: params[:id]).first
    if receiver.nil?
      redirect_to root_path
    else
      message = Message.new(receiver_id: params[:id])
      receiverid = params[:id]
      render locals: {
        message:  message, receiver: receiverid
      }
    end
  end

  def create
    @message = if current_user
                 current_user.sent_private_messages.build(message_params)
               else
                 Message.new(message_params)
               end

    respond_to do |format|
      if @message.save
        format.html { redirect_to @message, info: 'Сообщение отправленно.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new,  locals: { message: @message, receiver: @message.receiver_id } }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def message_params
    params.require(:message).permit(:body, :email, :receiver_id)
  end
end
