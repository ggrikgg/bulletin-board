# frozen_string_literal: true

class ItemsController < ApplicationController
  before_action :correct_user,   only: %i[edit update destroy sold unsold]
  before_action :ban_user, only: %i[new]
  before_action :set_item, only: %i[show edit update destroy sold unsold]

  # GET /items
  # GET /items.json
  def index
    @items = Item.where(sold: false).order(created_at: :DESC).paginate(page: params[:page], per_page: 16)
  end

  # GET /items/1
  # GET /items/1.json
  def show; end

  # GET /items/new
  def new
    @item = Item.new
    @testcat = Item.test_draw
  end

  # GET /items/1/edit
  def edit
    @testcat = Item.test_draw
  end

  # POST /items
  # POST /items.json
  def create
    @item = current_user.items.build(item_params)
    @testcat = Item.test_draw
    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, success: 'Объявление успешно создано.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, info: 'Объявление успешно изменено.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def sold
    @item.update(sold: true)
    redirect_to @item, info: 'Товар помечен как проданный.'
  end

  def unsold
    @item.update(sold: false)
    redirect_to @item, info: 'Товар помечен как активный'
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, info: 'Объявление удалено.' }
      format.json { head :no_content }
    end
  end

  private

  def correct_user
    @user = Item.find(params[:id]).user
    redirect_to(root_url) unless (@user == current_user) || current_user.admin?
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_item
    @item = Item.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_params
    params.require(:item).permit(:name, :sold, :category_id, :price, :description, :image, attachments: [])
  end

  def ban_user
    redirect_to(root_url) unless current_user && !current_user.ban?
  end
end
