# frozen_string_literal: true

class CallbacksController < Devise::OmniauthCallbacksController
  def vkontakte
    @user = User.find_for_vkontakte_oauth(request.env['omniauth.auth'])
    if @user.persisted?
      set_flash_message(:notice, :success, kind: 'Vkontakte') if is_navigational_format?
      sign_in_and_redirect @user, event: :authentication
    else
      session['devise.vkontakte_data'] = request.env['omniauth.auth'].except('extra')
      redirect_to new_user_registration_url
    end
  end
end
