# frozen_string_literal: true

class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
  #  before_action :authenticate_user!  :only => [:index]
  add_flash_types :danger, :info, :warning, :success
  private

  def admin_user
  	redirect_to(root_url) unless current_user && current_user.admin?
  end
end
