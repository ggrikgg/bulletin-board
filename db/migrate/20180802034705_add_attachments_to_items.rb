class AddAttachmentsToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :attachments, :json
  end
end
