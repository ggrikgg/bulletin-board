# frozen_string_literal: true

class AddUserIdToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :user_id, :integer
    add_index :items, %i[user_id created_at]
  end
end
