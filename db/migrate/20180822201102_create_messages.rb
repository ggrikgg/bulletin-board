class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string :name
      t.text :body, presence: true
      t.references :sender, index: true
      t.references :receiver, index: true, presence: true
      t.string :email
    end
  end
end
