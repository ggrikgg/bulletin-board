class AddReferenceToItem < ActiveRecord::Migration[5.1]
  def change
    add_reference :items, :category, index: true, foreign_key: true, null: false
  end
end
