# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'callbacks' }

  resources :messages, only: %i[create]
  get '/messages/new/:id', to: 'messages#new', as: 'new_message'

  authenticate :user do
    get 'new_category', to: 'categories#new', as: 'new_category'
    get 'messages/inbox', to: 'messages#index', as: 'inbox'
    get 'messages/outbox', to: 'messages#index_outbox', as: 'outbox'
    resources :items, only: %i[new create edit update destroy] do
      member do
        patch 'sold'
        patch 'unsold'
      end
    end
    resources :messages, only: %i[index]
    resources :users, only: %i[index]
    patch 'users/:id', to: 'users#change', as: 'admin_change_user'
    resources :categories, only: %i[create]
  end

  resources :items, only: %i[index show]

  resources :messages, only: %i[show]
  resources :categories, only: %i[index], as: 'categories'

  # get 'users/:id', to: 'users#show', as: 'user'

  resources :users, only: %i[show] do
    member do
      patch 'ban'
      patch 'unban'
      patch 'admin'
    end
  end
  get '/categories/*path', to: 'categories#categ_index', param: :path

  root 'items#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
